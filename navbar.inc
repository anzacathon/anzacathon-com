<header>
    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
      <a class="navbar-brand" href="/">Anzacathon</a>
      <div class="container">
        <div class="navbar-header">
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
        </div>
        <div class="collapse navbar-collapse" id="navbarCollapse">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item"><a href="/contact.shtml">Contact</a>&nbsp;</li>
            <li class="nav-item"><a href="/data-sources.shtml">Data sources</a>&nbsp;</li>
            <li class="nav-item"><a href="/faq.shtml">FAQ</a>&nbsp;</li>
            <li class="nav-item"><a href="https://gitlab.com/anzacathon">Gitlab page</a>&nbsp;</li>
            <li class="nav-item"><a href="/links.shtml">Links</a>&nbsp;</li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
</header>
